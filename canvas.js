var canvas = document.getElementById('canvas');
var ctx = canvas.getContext('2d');  
var dragging = false;
var startPos = {
    x: 100,
    y: 100
};
var shot;
var canvasHistory = [];
var index = 0;
var selected = document.querySelector('.selected');
var option = document.querySelector('#option');
var btn = document.querySelectorAll('button'); 

// save empty picture
window.onload = function(){
    canvasHistory.push(ctx.getImageData(0, 0, canvas.width, canvas.height));
    ctx.font = font_size.value + "px" + " " + font_family.value;
}

// change width size
var width_range = document.querySelector('#width_range');
width_range.addEventListener('change',function(){
    ctx.lineWidth = width_range.value;
});

// change color
var color = document.querySelector('#color');
var border_color = document.querySelector('#color_div');
color.addEventListener('change',function(){
    border_color.style.borderColor = color.value;
    ctx.strokeStyle = color.value;
    ctx.fillStyle = color.value;
});

// change mouse icon
var brush = document.querySelector('#brush');
brush.addEventListener('click',function(){
    selected.classList.remove('selected');
    this.classList.add('selected');
    selected = this;
    canvas.style.cursor = "url('cursor/brush.cur'),auto";
});

var circle = document.querySelector('#circle');
circle.addEventListener('click',function(){
    selected.classList.remove('selected');
    this.classList.add('selected');
    selected = this;
    canvas.style.cursor = "url('cursor/hollow_circle.cur'),auto";
});

var rectangle = document.querySelector('#rectangle');
rectangle.addEventListener('click',function(){
    selected.classList.remove('selected');
    this.classList.add('selected');
    selected = this;
    canvas.style.cursor = "url('cursor/hollow_rectangle.cur'),auto";
});

var triangle = document.querySelector('#triangle');
triangle.addEventListener('click',function(){
    selected.classList.remove('selected');
    this.classList.add('selected');
    selected = this;
    canvas.style.cursor = "url('cursor/hollow_triangle.cur'),auto";
});

var text = document.querySelector('#text');
text.addEventListener('click',function(){
    selected.classList.remove('selected');
    this.classList.add('selected');
    selected = this;
    canvas.style.cursor = "url('cursor/text.cur'),auto";
    text_input.value = "";
    startPos.x = 100;
    startPos.y = 100;
});

var eraser = document.querySelector('#eraser');
eraser.addEventListener('click',function(){
    selected.classList.remove('selected');
    this.classList.add('selected');
    selected = this;
    canvas.style.cursor = "url('cursor/eraser.cur'),auto";
});

var rainbow = document.querySelector('#rainbow');
rainbow.addEventListener('click',function(){
    selected.classList.remove('selected');
    this.classList.add('selected');
    selected = this;
    canvas.style.cursor = "url('cursor/rainbow.cur'),auto";
});

// draw
function getMousePos(event) {
    var x = event.clientX - canvas.getBoundingClientRect().left;
    var y = event.clientY - canvas.getBoundingClientRect().top;
    return {x: x, y: y};
}

function saveShot() {
    shot = ctx.getImageData(0, 0, canvas.width, canvas.height);
}

function restoreShot() {
    ctx.putImageData(shot, 0, 0);
}

function dragStart(event) {
    if(selected != text){
        dragging = true;
    }
    startPos = getMousePos(event);
    text_input.value = "";
    if(rainbow.classList.contains('selected')){
        ctx.beginPath();
        ctx.moveTo(startPos.x, startPos.y+32);
    }
    else {
        ctx.strokeStyle = color.value;
        ctx.fillStyle = color.value;
        if(brush.classList.contains('selected')){
            ctx.beginPath();
            ctx.moveTo(startPos.x, startPos.y+32);
        }
        else{
            saveShot();
        }
    }
}

function drawLine(position) {
    if(selected == rainbow){
        var grd=ctx.createLinearGradient(startPos.x,startPos.y,position.x,position.y);
        grd.addColorStop(0,"red");
        grd.addColorStop(0.2,"orange");
        grd.addColorStop(0.3,"yellow");
        grd.addColorStop(0.5,"green");
        grd.addColorStop(0.7,"blue");
        grd.addColorStop(0.9,"indigo");
        grd.addColorStop(1,"purple");
        ctx.strokeStyle = grd;
    }
    ctx.lineTo(position.x, position.y+32);
    ctx.stroke();
}

function drawCircle(position) {
    var r = Math.sqrt(Math.pow((startPos.x - position.x), 2) + Math.pow((startPos.y - position.y), 2))/2;
    ctx.beginPath();
    ctx.arc(startPos.x, startPos.y, r, 0, 2 * Math.PI);
}

function drawRectangle(position) {
    var w = position.x - startPos.x;
    var h = position.y - startPos.y;
    ctx.beginPath(); 
    ctx.rect(startPos.x,startPos.y,w,h); 
}

function drawTriangle(position){
    ctx.beginPath();
    ctx.moveTo(startPos.x, startPos.y);
    ctx.lineTo(position.x,position.y);
    var diff_x = position.x - startPos.x;
    ctx.lineTo(startPos.x-diff_x, position.y);
    ctx.closePath();
}

function draw(position, shape) {
    if (shape == "circle") {
        drawCircle(position);
    }
    else if (shape == "line") {
        drawLine(position);
    }
    else if(shape == "rectangle") {
        drawRectangle(position);
    }
    else{
        drawTriangle(position);
    }
    if(shape != "line"){
        if(option.value=="hollow"){
            ctx.stroke();
        }
        else{
            ctx.fill();
        }
    }
}

function erase(position) {
    ctx.clearRect(startPos.x,startPos.y+10, 25, 25);
    startPos.x = position.x;
    startPos.y = position.y;
}

function drag(event) {
    var position;
    if (dragging == true && selected != text) {
        position = getMousePos(event);
        if(brush.classList.contains('selected') || rainbow.classList.contains('selected')){
            draw(position, "line");
        }
        else{
            if(eraser.classList.contains('selected')){
                erase(position);
            }
            else{
                restoreShot();
                if(circle.classList.contains('selected')){
                    draw(position, "circle");
                }
                else if(rectangle.classList.contains('selected')){
                    draw(position, "rectangle");
                }
                else{
                    draw(position, "triangle");
                }
            }
            
        }
    }
}

function dragStop(event) {
    if(selected != text){
        dragging = false;
        index++;
        canvasHistory.length = index;
        canvasHistory.push(ctx.getImageData(0, 0, canvas.width, canvas.height));
    }
}

canvas.addEventListener('mousedown', dragStart);
canvas.addEventListener('mousemove', drag);
canvas.addEventListener('mouseup', dragStop);

// input text
var _text = document.querySelector('#Text');
var text_input = document.querySelector('#text_input');
var font_size = document.querySelector('#font_size');
var font_family = document.querySelector('#font_family');

btn.forEach(function(event){
    event.addEventListener('click',function(){
        if(selected == text){
            _text.style.display = "block";
        }
        else{
            _text.style.display = "none";
        }
    });
});

text_input.addEventListener('keydown',function(event){
    if(event.keyCode == 13){
        if(option.value == "hollow"){
            ctx.strokeText(text_input.value,startPos.x,startPos.y);
        }
        else{
            ctx.fillText(text_input.value,startPos.x,startPos.y);
        }
        index++;
        canvasHistory.length = index;
        canvasHistory.push(ctx.getImageData(0, 0, canvas.width, canvas.height));
    }
});

font_size.addEventListener('change',function(){
    ctx.font = font_size.value + "px" + " " + font_family.value;
});
font_family.addEventListener('change',function(){
    ctx.font = font_size.value + "px" + " " + font_family.value;
});

// download
var save = document.querySelector("#save");
save.addEventListener('click',function(){
    var _url = canvas.toDataURL();
    this.href = _url;
});

// upload
var upload = document.querySelector('#upload');
upload.addEventListener('change',function(){
    var _file = this.files[0];
    var img = new Image();
    img.src = window.URL.createObjectURL(_file)
    img.onload = function(){
        ctx.drawImage(img,100,100,200,200);
        index++;
        canvasHistory.length = index;
        canvasHistory.push(ctx.getImageData(0, 0, canvas.width, canvas.height));
    }
});

// undo
var undo = document.querySelector("#undo");
undo.addEventListener('click',function(){
    if(index > 0) {
        index--;
        ctx.putImageData(canvasHistory[index], 0, 0);
    }
});

// redo
var redo = document.querySelector("#redo");
redo.addEventListener('click',function(){
    if (index < canvasHistory.length - 1) {
        index++;
        ctx.putImageData(canvasHistory[index], 0, 0);
    }
});

// reset
var rst = document.querySelector("#rst");
rst.addEventListener('click',function(){
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    index++;
    canvasHistory.length = index;
    canvasHistory.push(ctx.getImageData(0, 0, canvas.width, canvas.height));
    console.log('clear');
});