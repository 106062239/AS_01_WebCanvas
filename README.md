# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here

## Init
* window.onload()，先儲存一張空白的畫布，初始化文字的字形和大小。

## Change width size
* 改變字、筆刷、圖形的寬度。

## Change color
* 改變字、筆刷、圖形的顏色，且按鈕的外框會顯示目前選取的顏色。
* 如果選了以後顏色沒改，應該是因為最右邊那條預設是最深色，條高就會改變了。

## Change mouse icon
* 幫某些按鈕註冊點擊事件，改變滑鼠游標的圖案，且改變被選按鈕的背景色。
* 能畫畫的按鈕、橡皮擦或打字才會有效果。

## Draw
* getMousePos()，取得現在的滑鼠位置。
* saveShot()，儲存現在的畫面結果。
* restoreShot()，回到上一次儲存的畫面結果。
* dragStart()，滑鼠在畫布上點下去的時候，存取初始滑鼠位置，根據所選的按鈕，做不同的操作。
* drag()，滑鼠點擊不放拖曳時，獲取當前滑鼠位置，根據所選的按鈕，做不同的操作。
* draw(position, shape)，根據drag()傳進來的結果，判斷要畫什麼圖形，且判斷Figure Style的狀態，決定是否填滿。
* drawLine(position)，傳入當前滑鼠位置，配合初始座標，畫線，如果點選的是彩虹，線會呈現七彩顏色。
* drawCircle(position)，傳入當前滑鼠位置，配合初始座標，畫圓。
* drawRectangle(position)，傳入當前滑鼠位置，配合初始座標，畫矩形。
* drawTriangle(position)，傳入當前滑鼠位置，配合初始座標，畫三角形。
* erase(position)，清除一小塊區域，且將初始位置改為現在位置。
* dragStop()，畫完放開滑鼠，如果所選按鈕不是打字，就將現在的畫面狀態存到一個陣列裡；如果是有用過Undo，會將現在陣列中的位置定為最新，例如history原本長度是4，undo到2，畫完新的後長度變3(index=2)，那
原本history[3]的會消失，這是我在電腦的小畫家實測的結果。

## Input text
* 將每個按鈕註冊點擊事件，如果按鈕是打字，就顯示出輸入欄，否則就不顯示。
* 將輸入欄註冊鍵盤輸入事件，如果輸入欄按下Enter，判斷Figure Style的狀態，決定是否填滿並畫出文字，將畫面結果存進陣列裡。
* 將font-size和font-family註冊change事件，被改變時，更改canvas的字體和大小。
* 點選打字按鈕後，可以在畫布中選自己想要的位置顯示文字(在那個位置點一下)，否則預設會出現在座標
(100,100)。

## Download
* 將畫布下載成圖片。

## Upload
* 上傳圖片，顯示在座標(100,100)，大小:200x200，將結果存到陣列。

## Undo
* 減少index，從陣列中抓取上一步的結果。

## Redo
* 增加index，回到陣列下一步的結果。

## Reset
* 清空畫面，且將現在狀態儲存到陣列，這樣如果按錯清空可以回復。

## 上面是介紹function，下面是介紹按鈕的效果

## Button Introduction
* Figure Style 可以選兩種狀態，Hollow和Filled，Hollow表示畫出來的圖形和文字會是空心的，Filled表示畫出來的圖形和文字是實心填滿的，筆刷和彩虹不會受到這個影響。
* 長得像調色盤的按鈕可以更換筆刷、圖形、文字顏色，選完顏色後，border的顏色會變成所選的顏色，如果選完沒改顏色，可能是最右邊那條預設是最低，調高就會改變了。
* 一個鉛筆圖案右邊一條可拖曳的軸是代表畫出來的東西寬度，筆刷、圖形(空心狀態時的外框寬度)、文字、彩虹寬度都會隨之改變。越往右越粗。
* 再來是代表字體的樣式和字體的大小。
* 長得像筆刷的按鈕代表筆刷，可以畫線條，有特別調整到從滑鼠icon的筆尖開始畫。
* 圓圈按鈕代表畫圓，矩形按鈕代表畫矩形，三角形按鈕代表畫三角形。
* 長得像輸入框的按鈕代表可以打字，點下去後會出現一個輸入欄，可以打字，可以將滑鼠移到畫布上，點一下畫布任意的地方，輸入的文字就會顯示在那邊，如果沒有點擊畫布，會預設出現在座標(100,100)。
* 長的像橡皮擦的按鈕就是橡皮擦，在畫布上按住拖移就可以擦了，有特別調整到從滑鼠icon的橡皮擦頭開始擦。
* 長的像彩虹的按鈕可以畫出七彩的顏色，用法和筆刷一樣，只是顏色不會受到調色盤按鈕的影響。
* 兩個箭頭繞成圓的按鈕代表reset，可以清空畫布，且會儲存現在的狀態，可以undo和redo。
* 向左的箭頭按鈕代表undo，可以回到上一步。
* 向右的箭頭按鈕代表redo，可以回到下一步。
* 箭頭往上的按鈕代表上傳，可以上傳圖片到座標(100,100)的位置，大小是200x200。
* 箭頭往下的按鈕代表下載，可以將畫布下載成一張圖片。



